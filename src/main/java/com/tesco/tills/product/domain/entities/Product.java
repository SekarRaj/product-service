package com.tesco.tills.product.domain.entities;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
public class Product {
    private String productId;

    private String category;

    private String department;

    private String productName;

    private String unitOfMeasure;

    private Double pricePerItem;
}
