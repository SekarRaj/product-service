package com.tesco.tills.product.exception;

import lombok.Getter;

@Getter
public enum ExceptionConstants {
    PRODUCT_NOT_FOUND_ID("No Product found with id "),
    PRODUCT_NOT_FOUND_NAME("No Product found with name ");

    private String message;

    ExceptionConstants(String message) {
        this.message = message;
    }
}
