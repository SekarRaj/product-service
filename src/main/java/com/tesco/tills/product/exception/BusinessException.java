package com.tesco.tills.product.exception;

/**
 * @author Ravi Raizada
 * @version 1.0
 */
public class BusinessException extends RuntimeException {

    public BusinessException(String message) {
        super(message);
    }
}
