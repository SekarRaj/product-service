package com.tesco.tills.product.controller;

import com.tesco.tills.product.domain.entities.Product;
import com.tesco.tills.product.exception.ProductNotFoundException;
import com.tesco.tills.product.service.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ProductController {
    ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @ApiOperation(value = "Product Information by product id", notes = "This API is used for getting product information by passing product id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product is found"),
            @ApiResponse(code = 500, message = "Internal error")})
    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping(value = "/id/{productId}")
    public Product findById(@PathVariable("productId") String productId) throws ProductNotFoundException {
        return productService.findById(productId);
    }

    @ApiOperation(value = "Product Information by product name", notes = "This API is used for getting product information by passing product name")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product is found"),
            @ApiResponse(code = 500, message = "Internal error")})
    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping("/name/{productName}")
    public Product findByName(@PathVariable("productName") String productName) throws ProductNotFoundException {
        return productService.findByName(productName);
    }
}
