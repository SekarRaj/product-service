package com.tesco.tills.product.repository;

import com.tesco.tills.product.domain.entities.Product;
import org.springframework.stereotype.Service;

@Service
public class ProductRepository {
    public Product findById(String id) {
        if (id.equals("123"))
            return Product.builder()
                    .category("Grocery")
                    .department("Perishable")
                    .pricePerItem(2.00)
                    .productId(id)
                    .productName("Spinach and Greens")
                    .build();
        else return null;
    }

    public Product findByName(String description) {
        if (description.equalsIgnoreCase("JAM"))
            return Product.builder()
                    .category("Canned Food")
                    .department("Preservatives")
                    .pricePerItem(11.00)
                    .productId("ID12")
                    .productName(description)
                    .build();
        else return null;
    }
}
