package com.tesco.tills.product.service;

import com.tesco.tills.product.exception.ExceptionConstants;
import com.tesco.tills.product.exception.ProductNotFoundException;
import com.tesco.tills.product.repository.ProductRepository;
import com.tesco.tills.product.domain.entities.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ProductRepositoryService implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Product findById(String id) throws ProductNotFoundException {
        Product response = productRepository.findById(id);
        if (response != null) {
            return response;
        } else {
            throw new ProductNotFoundException(ExceptionConstants.PRODUCT_NOT_FOUND_ID.getMessage() + id);
        }
    }

    public Product findByName(String name) throws ProductNotFoundException {
        Product response = productRepository.findByName(name);
        if (response != null) {
            return response;
        } else {
            throw new ProductNotFoundException(ExceptionConstants.PRODUCT_NOT_FOUND_NAME.getMessage() + name);
        }
    }
}
