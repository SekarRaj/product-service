package com.tesco.tills.product.service;

import com.tesco.tills.product.exception.ProductNotFoundException;
import com.tesco.tills.product.domain.entities.Product;
import org.springframework.stereotype.Service;

@Service
public interface ProductService {

    Product findById(String item) throws ProductNotFoundException;

    Product findByName(String description) throws ProductNotFoundException;
}

