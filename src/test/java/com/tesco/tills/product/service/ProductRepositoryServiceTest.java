package com.tesco.tills.product.service;

import com.tesco.tills.product.domain.entities.Product;
import com.tesco.tills.product.exception.ProductNotFoundException;
import com.tesco.tills.product.repository.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductRepositoryServiceTest {

    @InjectMocks
    ProductRepositoryService service;

    @Mock
    ProductRepository productRepository;

    @Test
    public void findById_shouldReturnProductWhenProductExists() {
        when(productRepository.findById("123")).thenReturn(getProduct());

        Product product = service.findById("123");

        assertNotNull(product);
    }

    @Test(expected = ProductNotFoundException.class)
    public void findById_shouldThrowNotFoundExceptionWhenProductNotFound() {
        when(productRepository.findById(anyString())).thenReturn(null);

        service.findById("456");
    }

    @Test
    public void findByName() {
        when(productRepository.findByName(anyString())).thenReturn(getProduct());

        Product product = service.findByName("Jam");

        assertNotNull(product);
    }

    @Test(expected = ProductNotFoundException.class)
    public void findByName_shouldThrowNotFoundExceptionWhenProductNotFound() {
        when(productRepository.findByName(anyString())).thenReturn(null);

        service.findByName("Sauce");
    }

    private Product getProduct() {
        return Product.builder()
                .productName("pName")
                .productId("pid")
                .pricePerItem(0.00)
                .department("pDept")
                .category("pCat")
                .unitOfMeasure("UOM")
                .build();
    }
}