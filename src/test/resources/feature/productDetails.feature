Feature: Get Product details

  Scenario: Product is present in database
    Given Customer has given me an product id "20000207002"
    When Product is present in database
    Then Product details should be returned

  Scenario: Product is not present in database
    Given Customer has given me an product id "20000207273"
    When Product is not present in database
    Then Product not found exception should be thrown