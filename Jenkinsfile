pipeline {
//    agent { label 'docker-java-agent' }

    agent any

    tools {
        maven 'Maven'
    }

    environment {
        NEXUS_VERSION = "nexus3"
        NEXUS_PROTOCOL = "http"
        NEXUS_URL = "52.66.213.4:8081"
        NEXUS_REPOSITORY = "jets"
        NEXUS_CREDENTIAL_ID = "nexus-id"
    }

    stages {

        stage('Build and Package') {
            steps {
                sh "mvn -B -DskipTests clean package"
            }
        }

        stage('Build and Push Docker Image') {
            steps {
                sh "mvn compile jib:build"
            }
        }

        stage('Unit Test') {
            steps {
                sh 'mvn test'
            }
            post {
                always {
                    junit 'target/surefire-reports/*.xml'
                }
            }
        }

        stage('Test Report') {
            steps {
                junit '**/target/surefire-reports/*.xml'
            }
        }

        stage('Sonarqube') {
            steps {
                withSonarQubeEnv('sonarqube') {
                    sh  'mvn sonar:sonar \
                        -Dsonar.projectKey=jets \
                        -Dsonar.host.url=http://sonar-dev-1310005003.us-east-2.elb.amazonaws.com \
                        -Dsonar.login=699c9a74d2d091fc6ed93f47d3bdf310581f1e78'
                }

                timeout(time: 10, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }

        stage('Publish Artifact') {
            steps {
                script {
                    pom = readMavenPom file: "pom.xml"
                    artifact = findFiles(glob: "target/*.${pom.packaging}")[0]
                    artifactPath = artifact.path

                    echo "${pom.packaging}"
                    echo "${artifactPath}"

                    artifactExists = fileExists artifactPath

                    if (artifactExists) {
                        echo "*** Artifiact: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}"


                        nexusArtifactUploader(
                                nexusVersion: NEXUS_VERSION,
                                protocol: NEXUS_PROTOCOL,
                                nexusUrl: NEXUS_URL,
                                groupId: pom.groupId,
                                version: "${pom.version}-${GIT_COMMIT}",
                                repository: NEXUS_REPOSITORY,
                                credentialsId: NEXUS_CREDENTIAL_ID,
                                artifacts: [
                                        // Artifact generated such as .jar, .ear and .war files.
                                        [artifactId: pom.artifactId,
                                         classifier: '',
                                         file      : artifactPath,
                                         type      : pom.packaging],

                                        // Lets upload the pom.xml file for additional information for Transitive dependencies
                                        [artifactId: pom.artifactId,
                                         classifier: '',
                                         file      : "pom.xml",
                                         type      : "pom"]
                                ]
                        )
                    }
                }
            }
        }

        stage('Deploy to Openshift') {
            steps {
               // sh 'make deploy-oc'
               build job: 'deployment', parameters: [string(name: 'Service Name ', value: pom.artifactId),
                                                    string(name: 'environment ', value: GIT_COMMIT)]
            }
        }
    }


}
